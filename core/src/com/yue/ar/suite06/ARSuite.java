package com.yue.ar.suite06;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.Viewport;

public class ARSuite extends ApplicationAdapter {

	OrthographicCamera camera;
	Viewport viewport;

	Sprite sprite, sprite2;
	SpriteBatch batch;
	float rotation = 2;
	Rectangle rec, rec2;

	Texture img;
	
	@Override
	public void create () {
		camera = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		camera.setToOrtho(true,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

		sprite = new Sprite(new Texture(Gdx.files.internal("badlogic.jpg")));
		sprite2 = new Sprite(new Texture(Gdx.files.internal("badlogic.jpg")));
		// 设置(320, 240)为sprite的中心点，并且以它为中心绘制
		sprite.setCenter(320, 240);
		sprite.flip(false,true);

		sprite.setOriginCenter();  // 设置旋转中心在中心
		rec = sprite.getBoundingRectangle();  // 后面做碰撞检测用

		sprite2.setSize(60, 60);  // 设置精灵大小为40*40
		sprite2.flip(false,true);
		batch = new SpriteBatch();
	}

	@Override
	public void render () {

		camera.update();
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		float dt = Gdx.graphics.getDeltaTime();
		rotation += dt / 360;
		sprite.rotate(rotation);

		// 每秒钟移动20
		sprite2.translate(20 * dt, 20 * dt);

		rec2 = sprite2.getBoundingRectangle();
		if (rec2.overlaps(rec)) {
			System.out.println("conflict");
			sprite.setAlpha(0.5f);
			sprite2.setAlpha(1.0f);
			sprite2.setScale(2.0f);
		}

		batch.begin();
		sprite.draw(batch);
		sprite2.draw(batch, 0.8f);
		batch.end();
	}
	
	@Override
	public void dispose () {
		sprite.getTexture().dispose();
		sprite2.getTexture().dispose();
		batch.dispose();
	}
}
